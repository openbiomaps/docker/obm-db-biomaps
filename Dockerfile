FROM  mdillon/postgis:9.6

ENV DB=biomaps

ENV POSTGRES_USER ${DB}
ENV POSTGRES_PASSWORD changeMe

ADD https://openbiomaps.org/projects/checkitout/template.php?sql&${DB} /docker-entrypoint-initdb.d/${DB}.sql
RUN chmod 444 /docker-entrypoint-initdb.d/${DB}.sql
